package thanh.app.examapp.DAO;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import thanh.app.examapp.Model.CauHoiModel;
import thanh.app.examapp.SQLHelper.SQLiteHelper;

public class CauHoiDAO {
    SQLiteDatabase database;
    SQLiteHelper sqLiteHelper;
    private Context context;

    public CauHoiDAO(Context context){

        this.context = context;
        sqLiteHelper=new SQLiteHelper(context);
    }

    public  void open(){
        database=sqLiteHelper.getWritableDatabase();
        database=sqLiteHelper.getReadableDatabase();
    }
    public void close(){
        database.close();
    }

    public List<CauHoiModel> getListCauHoi(int Made){
        List<CauHoiModel> dataList=new ArrayList<>();

        String queryCauHoi="SELECT * FROM CAUHOI WHERE MADE="+String.valueOf(Made);
        Log.e("Câu truy vấn câu hỏi",queryCauHoi);
        Cursor cursor=database.rawQuery(queryCauHoi,null,null);
        cursor.moveToFirst();
        Log.e("check cursor",String.valueOf(cursor.getCount()));
        while(!cursor.isAfterLast()){
            CauHoiModel cauHoiModel=new CauHoiModel();
            int MaDe=cursor.getInt(0);
            int MaCauHoi=cursor.getInt(1);
            String NoiDung=cursor.getString(2);

            dataList.add(new CauHoiModel(MaDe,MaCauHoi,NoiDung));
            cursor.moveToNext();
        }
        Log.e("size list",String.valueOf(dataList.size()));
        return dataList;
    }
}
