package thanh.app.examapp.Model;

public class CauHoiModel {
    public CauHoiModel(int made,int maCh, String noiDung) {
        this.MaCh=maCh;
        this.Made = made;
        this.NoiDung = noiDung;
    }
    public CauHoiModel() {
        Made=0;
        MaCh=0;
        NoiDung="Empty";
    }
    public int getMade() {
        return Made;
    }

    public void setMade(int made) {
        Made = made;
    }

    public int getMaCh() {
        return MaCh;
    }

    public void setMaCh(int maCh) {
        MaCh = maCh;
    }

    public String getNoiDung() {
        return NoiDung;
    }

    public void setNoiDung(String noiDung) {
        NoiDung = noiDung;
    }

    int Made;
    int MaCh;
    String NoiDung;
}
