package thanh.app.examapp.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import thanh.app.examapp.Model.DeModel;
import thanh.app.examapp.R;

public class DeAdapter extends RecyclerView.Adapter<DeAdapter.ViewHolder> {

    private final Context context;
    private final List<DeModel> listData;

    public DeAdapter(Context context, List<DeModel> listData){

        this.context = context;
        this.listData = listData;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_de,viewGroup,false));
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int i) {
        viewHolder.tv_tenDe.setText(listData.get(i).getTenDe()+" "+String.valueOf(listData.get(i).getMade()));
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.tv_tenDe)
        TextView tv_tenDe;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            tv_tenDe.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.tv_tenDe:
                    Toast.makeText(context, "Chuyển đến đề số "+String.valueOf(listData.get(getAdapterPosition()).getMade()), Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    }
}
