package thanh.app.examapp.DAO;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import thanh.app.examapp.Model.DeModel;
import thanh.app.examapp.SQLHelper.SQLiteHelper;

public class DeDAO {
    SQLiteDatabase database;
    SQLiteHelper sqLiteHelper;
    private Context context;

    public DeDAO(Context context){

        this.context = context;
        sqLiteHelper=new SQLiteHelper(context);
    }

    public  void open(){
        database=sqLiteHelper.getWritableDatabase();
        database=sqLiteHelper.getReadableDatabase();
    }
    public void close(){
        database.close();
    }

    public List<DeModel> getListDe(){
        List<DeModel> dataList=new ArrayList<>();

        String queryDe="SELECT * FROM DE";
        Log.e("Câu truy vấn câu hỏi",queryDe);
        Cursor cursor=database.rawQuery(queryDe,null,null);
        cursor.moveToFirst();
        Log.e("check cursor",String.valueOf(cursor.getCount()));
        while(!cursor.isAfterLast()){

            int MaDe=cursor.getInt(0);
            String TenDe=cursor.getString(1);

            dataList.add(new DeModel(MaDe,TenDe));
            cursor.moveToNext();
        }
        Log.e("size list",String.valueOf(dataList.size()));
        return dataList;
    }
}
