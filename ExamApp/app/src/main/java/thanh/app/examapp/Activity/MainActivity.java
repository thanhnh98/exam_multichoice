package thanh.app.examapp.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import thanh.app.examapp.Adapter.DeAdapter;
import thanh.app.examapp.DAO.CauHoiDAO;
import thanh.app.examapp.DAO.CauTraLoiDAO;
import thanh.app.examapp.DAO.DeDAO;
import thanh.app.examapp.Model.CauHoiModel;
import thanh.app.examapp.Model.CauTraLoiModel;
import thanh.app.examapp.Model.DeModel;
import thanh.app.examapp.R;

public class MainActivity extends AppCompatActivity {
    CauHoiDAO cauHoiDAO;
    DeDAO deDAO;
    CauTraLoiDAO cauTraLoiDAO;
    List<CauHoiModel> CauHoi_dataList;
    List<DeModel> De_dataList;
    List<CauTraLoiModel> CauTraLoi_dataList;

    DeAdapter De_Adapter;
    @BindView(R.id.rcv_listDe)
    RecyclerView rcv_listDe;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        cauHoiDAO=new CauHoiDAO(this);
        deDAO=new DeDAO(this);
        cauTraLoiDAO=new CauTraLoiDAO(this);
        CauHoi_dataList=new ArrayList<>();
        De_dataList=new ArrayList<>();
        CauTraLoi_dataList=new ArrayList<>();
        cauHoiDAO.open();
        deDAO.open();
        cauTraLoiDAO.open();
        //------------
        CauHoi_dataList=cauHoiDAO.getListCauHoi(1);
        De_dataList= deDAO.getListDe();
        CauTraLoi_dataList=cauTraLoiDAO.getListCauTraLoi(1);
        //------------------
        cauHoiDAO.close();
        deDAO.close();
        cauTraLoiDAO.close();
        //Log.e("size",String.valueOf(De_dataList.size()));
        De_Adapter=new DeAdapter(this,De_dataList);
        rcv_listDe.setLayoutManager(new LinearLayoutManager(this));
        rcv_listDe.setAdapter(De_Adapter);
        De_Adapter.notifyDataSetChanged();


    }
}
