package thanh.app.examapp.Model;

public class CauTraLoiModel {

    public int getMaCh() {
        return MaCh;
    }

    public void setMaCh(int maCh) {
        MaCh = maCh;
    }

    public int getMaCTL() {
        return MaCTL;
    }

    public void setMaCTL(int maCTL) {
        MaCTL = maCTL;
    }

    public String getNoiDung() {
        return NoiDung;
    }

    public void setNoiDung(String noiDung) {
        NoiDung = noiDung;
    }

    public int getCheck() {
        return Check;
    }

    public void setCheck(int check) {
        Check = check;
    }

    public CauTraLoiModel(int maCh, String noiDung, int check) {
        MaCh = maCh;
        NoiDung = noiDung;
        Check = check;
    }
    public CauTraLoiModel() {
        MaCh = 0;
        NoiDung = "Empty Answer";
        Check = 0;
    }

    int MaCh;

    int MaCTL;
    String NoiDung;
    int Check;
}
