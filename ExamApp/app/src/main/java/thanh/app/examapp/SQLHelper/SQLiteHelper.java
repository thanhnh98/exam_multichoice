package thanh.app.examapp.SQLHelper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class SQLiteHelper extends SQLiteOpenHelper {

    public static String DB_EXAM="EXAM";
    public static String TABLE_CAUHOI="CAUHOI";
    public static String TABLE_CAUTRALOI="CAUTRALOI";
    public static String TABLE_DE="DE";


    public SQLiteHelper( Context context) {
        super(context, DB_EXAM, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.e("OK","Create");
        String createCauHoi="CREATE TABLE \"CAUHOI\" (\n" +
                "\t\"MADE\"\tINTEGER,\n" +
                "\t\"MACH\"\tINTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,\n" +
                "\t\"NOIDUNG\"\tTEXT\n" +
                ");";
        String createCauTraLoi="CREATE TABLE \"CAUTRALOI\" (\n" +
                "\t\"MACH\"\tINTEGER,\n" +
                "\t\"MACTL\"\tINTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,\n" +
                "\t\"NOIDUNG\"\tTEXT,\n" +
                "\t\"CHECK\"\tINTEGER\n" +
                ");";
        String createDe="CREATE TABLE \"DE\" (\n" +
                "\t\"MADE\"\tINTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,\n" +
                "\t\"TENDE\"\tTEXT\n" +
                ");";
        db.execSQL(createCauHoi);
        db.execSQL(createCauTraLoi);
        db.execSQL(createDe);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.e("Updare","OK");
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_CAUHOI);
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_CAUTRALOI);
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_DE);
        onCreate(db);
    }
}
