package thanh.app.examapp.DAO;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import thanh.app.examapp.Model.CauTraLoiModel;
import thanh.app.examapp.SQLHelper.SQLiteHelper;

public class CauTraLoiDAO {
    SQLiteDatabase database;
    SQLiteHelper sqLiteHelper;
    private Context context;

    public CauTraLoiDAO(Context context){

        this.context = context;
        sqLiteHelper=new SQLiteHelper(context);
    }

    public  void open(){
        database=sqLiteHelper.getWritableDatabase();
        database=sqLiteHelper.getReadableDatabase();
    }
    public void close(){
        database.close();
    }

    public List<CauTraLoiModel> getListCauTraLoi(int MaCauHoi){
        List<CauTraLoiModel> dataList=new ArrayList<>();

        String queryCauTraLoi="SELECT * FROM CAUTRALOI WHERE MACH="+String.valueOf(MaCauHoi);
        Log.e("Câu truy vấn câu hỏi",queryCauTraLoi);
        Cursor cursor=database.rawQuery(queryCauTraLoi,null,null);
        cursor.moveToFirst();
        Log.e("check cursor",String.valueOf(cursor.getCount()));
        while(!cursor.isAfterLast()){
            CauTraLoiModel CauTraLoiModel=new CauTraLoiModel();
            int MaCH=cursor.getInt(0);
            String MaCTL=cursor.getString(1);
            String NoiDung=cursor.getString(2);
            int Check=cursor.getInt(3);

            dataList.add(new CauTraLoiModel(MaCH,NoiDung,Check));
            cursor.moveToNext();
        }
        Log.e("size list",String.valueOf(dataList.size()));
        return dataList;
    }
}
