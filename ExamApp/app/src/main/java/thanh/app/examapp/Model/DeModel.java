package thanh.app.examapp.Model;

public class DeModel {
    public int getMade() {
        return Made;
    }

    public void setMade(int made) {
        Made = made;
    }

    public String getTenDe() {
        return TenDe;
    }

    public void setTenDe(String tenDe) {
        TenDe = tenDe;
    }

    public DeModel(int Made, String tenDe) {
        this.Made= Made;
        this.TenDe = tenDe;
    }
    public DeModel() {
        TenDe = "Mã đề trống";
    }

    int Made;
    String TenDe;
}
